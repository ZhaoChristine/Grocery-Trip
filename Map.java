import java.awt.Graphics;

/** Map.java
 * @author Christine Zhao
 * Abstract map class with draw
 */
public abstract class Map {

  public abstract void draw(Graphics g);
} // end Map

