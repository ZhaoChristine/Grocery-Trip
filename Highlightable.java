/** Highlightable.java
 * @author Christine Zhao
 * Highlightable interface to highlight objects
 */
public interface Highlightable {
  public abstract void highlight ();
  public abstract void unHighlight();
} // end Highlightable
